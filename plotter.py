import pandas as pd
import matplotlib.pyplot as plt
import sys

filename, title = sys.argv[1:]

df_add = pd.read_csv(filename)

plt.plot(df_add["op_cnt"], df_add["time"])
plt.title(title)
plt.xlabel("Count")
plt.ylabel("Time")
plt.grid(True)
plt.show()

